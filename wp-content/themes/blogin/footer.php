        <footer class="footer center-xs">
            <!-- social icon widget -->
            <div id="footer-sidebar" class="secondary">
                <?php if(!dynamic_sidebar('footer-sidebar')) : ?>

                <?php endif; ?>
        </footer>

<?php wp_footer(); ?>

</body>
</html>