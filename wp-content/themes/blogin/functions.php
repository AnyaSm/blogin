<?php

function load_style() {
    wp_enqueue_style('bootstrap.min.css', get_template_directory_uri() . '/styles/css/bootstrap.min.css');
    wp_enqueue_style('flexboxgrid.min.css', get_template_directory_uri() . '/styles/flexboxgrid/css/flexboxgrid.min.css');
    wp_enqueue_style('font-awesome.min.css', get_template_directory_uri() . '/styles/font-awesome/css/font-awesome.min.css');
    wp_enqueue_style('hover.css', get_template_directory_uri() . '/styles/css/hover.css');
    wp_enqueue_style('main', get_template_directory_uri() . '/styles/css/styles.css');
}

add_action ('wp_enqueue_scripts', 'load_style');

function load_script() {
    wp_enqueue_script('jquery-1.12.0', get_template_directory_uri() . '/js/jquery-1.12.0.min.js');
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js');
    wp_enqueue_script('main', get_template_directory_uri() . '/js/main.js');
}

add_action ('wp_enqueue_scripts', 'load_script');

// Navigation Menus
register_nav_menu ('main-menu', 'header-menu');

// LOGO
function my_after_setup_theme() {
    add_image_size( 'my-theme-logo-size', auto, auto, true );
    add_theme_support( 'site-logo', array( 'size' => 'my-theme-logo-size' ) );
}
add_action( 'after_setup_theme', 'my_after_setup_theme' );

// поддержка миниаютр
add_theme_support('post-thumbnails');

//content
function content($limit) {
    $content = explode(' ', get_the_content(), $limit);
    if (count($content)>=$limit) {
        array_pop($content);
        $content = implode(" ",$content).'...';
    } else {
        $content = implode(" ",$content);
    }
    $content = preg_replace('/\[.+\]/','', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content;
}

/**
 * Register our sidebars and widgetized areas.
 *
 */
register_sidebar( array(
    'name'          => 'sidebar',
    'id'            => 'sidebar',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>',
    'description'   => 'create widgets here'
) );


function is_type_page() { // Check if the current post is a page
    global $post;

    if ($post->post_type == 'page') {
        return true;
    } else {
        return false;
    }
}

/**Footer sidebar **/

register_sidebar( array(
    'name' => 'Footer Sidebar',
    'id' => 'footer-sidebar',
    'description' => 'Appears in the footer area',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
) );
if ( function_exists('register_sidebar') );