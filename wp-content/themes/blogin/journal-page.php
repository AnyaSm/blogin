<?php
/*
Template Name: Journal Page
*/
get_header(); ?>
<div class="container">
    <section class="content">

        <?php
        // set up or arguments for our custom query (for pagination)
        $paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
        $query_args = array(
            'post_type' => 'post',
            'posts_per_page' => 1,
            'paged' => $paged
        );
        // create a new instance of WP_Query
        $the_query = new WP_Query( $query_args );
        ?>
        <?php if ( $the_query->have_posts() ) :
            while ( $the_query->have_posts() ) : $the_query->the_post(); // run the loop ?>

            <article class="post col-sm-8">
                <div class="img-wrap">
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('full', 'class=img-responsive'); ?>
                    </a>
                </div>
                <h2>
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                </h2>
                <?php echo content(43); ?>
                <a class="button hvr-grow" href="<?php the_permalink(); ?>">Continue Reading</a>
            </article>
        <?php endwhile; ?>

    <?php else: ?>
            <p>No posts found</p>
        <?php endif; ?>

        <?php get_sidebar(); ?>
    </section>

    <!-- post pagination-->
    <?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
        <nav class="prev-next-posts row">
            <div class="next-posts-link col-sm-6">
                <?php echo get_previous_posts_link( 'Previous' ); // display newer posts link ?>
            </div>
            <div class="prev-posts-link col-sm-6 end-xs">
                <?php echo get_next_posts_link( 'Next', $the_query->max_num_pages ); // display older posts link ?>
            </div>
        </nav>
    <?php } ?>

</div>

<?php get_footer(); ?>