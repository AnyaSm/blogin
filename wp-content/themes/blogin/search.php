<?php

get_header(); ?>
    <div class="container">
        <section class="content">
            <div class="col-sm-8">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php if (is_type_page()) continue; ?>

                        <article class="post">
                            <div class="img-wrap">
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_post_thumbnail('full', 'class=img-responsive'); ?>
                                </a>
                            </div>
                            <h2>
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h2>
                            <?php echo content(43); ?>
                            <a class="button hvr-grow" href="<?php the_permalink(); ?>">Continue Reading</a>
                        </article>
                    <?php endwhile; ?>

                <?php else: ?>
                    <h2 class="center">No posts found. Try a different search?</h2>
                    <?php include (TEMPLATEPATH . '/searchform.php'); ?>
                <?php endif; ?>
            </div>
            <?php get_sidebar(); ?>
        </section>
        <div class="pag-wrap">
            <?php
            global $wp_query;

                $big = 999999999; // need an unlikely integer

                echo paginate_links( array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'format' => '?paged=%#%',
                    'total' => $wp_query->max_num_pages,
                    'prev_text' => 'Previous',
                    'next_text' => 'Next'
                ) );
            ?>
        </div>
    </div>

<?php get_footer(); ?>