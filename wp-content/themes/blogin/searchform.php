<form name="search" action="<?php echo home_url( '/' ) ?>" method="get" class="search-form">
    <div class="form-wrap">
        <input type="text" placeholder="What are you looking for?" autocomplete="off" value="<?php echo get_search_query() ?>"
           name="s" id="text" class="input">
    </div>
</form>