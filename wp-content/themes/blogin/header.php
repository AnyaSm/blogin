<!DOCTYPE html>
<html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width">
        <title><?php bloginfo('name'); ?></title>
        <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
        <?php wp_head(); ?>
    </head>

<body <?php body_class(); ?>>
        <header class="header">
            <nav class="navbar navbar-inverse row middle-xs">
                <div class="container">
                    <div class="navbar-header col-sm-4">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <h1>
                            <?php if ( function_exists( 'jetpack_the_site_logo' ) ) jetpack_the_site_logo(); ?>
                        </h1>
                    </div>
                    <div id="navbar" class="collapse navbar-collapse">
                    <?php wp_nav_menu( array(
                        'theme_location'  => 'main-menu',
                        'container'       => false,
                        'menu_class'      => 'nav navbar-nav main-menu-list row col-sm-8 end-xs'
                        ));
                    ?>
                    </div><!--/.nav-collapse -->
                </div>
            </nav>
        </header>